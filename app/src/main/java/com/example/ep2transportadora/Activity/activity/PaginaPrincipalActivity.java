package com.example.ep2transportadora.Activity.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.ep2transportadora.Activity.adapter.VeiculosAdapter;
import com.example.ep2transportadora.Activity.model.Veiculos;
import com.example.ep2transportadora.R;

import java.util.ArrayList;
import java.util.List;

public class PaginaPrincipalActivity extends AppCompatActivity {

    private Button buttonAddVeiculo;
    private Button buttonFrete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_principal);

        buttonAddVeiculo = findViewById(R.id.idAddVeiculo);
        buttonFrete = findViewById(R.id.idFrete);

        //Evento de click no botão de Adicionar veículos
        buttonAddVeiculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selecionar = new Intent(getApplicationContext(), AddVeiculoActivity.class);
                startActivity(selecionar);

            }
        });
        //Evento de click no botão do frete
        buttonFrete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent frete = new Intent(getApplicationContext(), FreteActivity.class);
                startActivity(frete);


            }
        });
    }
}
