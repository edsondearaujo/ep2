package com.example.ep2transportadora.Activity.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ep2transportadora.Activity.model.Veiculos;

import java.util.List;

public class VeiculoDAO implements IVeiculoDAO {

    private SQLiteDatabase escreve;
    private SQLiteDatabase le;


    public VeiculoDAO(Context context) {
        DbHelper db = new DbHelper(context);
        escreve = db.getWritableDatabase();
        le = db.getReadableDatabase();
    }

    @Override
    public boolean salvar(Veiculos veiculo) {

        ContentValues cv = new ContentValues();
        cv.put("nome", veiculo.getNomeVeiculo());

        try{
            escreve.insert(DbHelper.TABELA_VEICULOS, null, cv );
            Log.e("INFO", "veículo salvo com sucesso!" );

        }catch (Exception e){
            Log.e("INFO", "Erro ao salvar veículo" + e.getMessage());


        }
        return false;
    }

    @Override
    public boolean atualizar(Veiculos veiculo) {
        return false;
    }

    @Override
    public boolean deletar(Veiculos veiculo) {
        return false;
    }

    @Override
    public List<Veiculos> listar() {
        return null;
    }
}
