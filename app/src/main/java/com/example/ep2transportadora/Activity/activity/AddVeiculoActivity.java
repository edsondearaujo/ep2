package com.example.ep2transportadora.Activity.activity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ep2transportadora.Activity.adapter.VeiculosAdapter;
import com.example.ep2transportadora.Activity.helper.DbHelper;
import com.example.ep2transportadora.Activity.helper.RecyclerItemClickListener;
import com.example.ep2transportadora.Activity.helper.VeiculoDAO;
import com.example.ep2transportadora.Activity.model.Veiculos;
import com.example.ep2transportadora.R;

import java.util.ArrayList;
import java.util.List;

public class AddVeiculoActivity extends AppCompatActivity {

    //private RecyclerView recyclerView;
    private VeiculosAdapter veiculosAdapter;
    private List<Veiculos> listaDeVeiculos = new ArrayList<>();
    private TextInputEditText editVeiculo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_veiculo);

        editVeiculo = findViewById(R.id.idDigVeiculo);

        //Configurar recycler
        //recyclerView = findViewById(R.id.idrecyclerView);

        //Adicionar evento de clique
        /*recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Log.i("clique", "onItemClick");
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                Log.i("clique", "onLongItemClick");
                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );*/
    }
    public void carregarFrota(){

        //Lista de veiculos
        Veiculos veiculo1 = new Veiculos();
        veiculo1.setNomeVeiculo("carreta");
        listaDeVeiculos.add(veiculo1);

        Veiculos veiculo2 = new Veiculos();
        veiculo2.setNomeVeiculo("van");
        listaDeVeiculos.add(veiculo2);


        /*
        Exibir lista de veiculos nno RecyclerView
         */


        //configurar adapter
        veiculosAdapter = new VeiculosAdapter(listaDeVeiculos);


        //Configurar RecyclerView
        /*RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        recyclerView.setAdapter(veiculosAdapter);*/
    }

    @Override
    protected void onStart() {
        carregarFrota();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Método para inflar o menu
        getMenuInflater().inflate(R.menu.menu_salvar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.itemSalvar:
                //Executar ação para o item salvar
                VeiculoDAO veiculoDAO = new VeiculoDAO(getApplicationContext());

                Veiculos veiculos = new Veiculos();
                veiculos.setNomeVeiculo("Carreta");
                veiculoDAO.salvar(veiculos);

                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
