package com.example.ep2transportadora.Activity.helper;

import com.example.ep2transportadora.Activity.model.Veiculos;

import java.util.List;

public interface IVeiculoDAO {

    public boolean salvar(Veiculos veiculo);
    public boolean atualizar(Veiculos veiculo);
    public boolean deletar(Veiculos veiculo);
    public List<Veiculos> listar();

}
