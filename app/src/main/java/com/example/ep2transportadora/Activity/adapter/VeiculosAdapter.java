package com.example.ep2transportadora.Activity.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ep2transportadora.Activity.model.Veiculos;
import com.example.ep2transportadora.R;

import java.util.List;

public class VeiculosAdapter extends RecyclerView.Adapter<VeiculosAdapter.MyViewHolder> {
    private List<Veiculos> listaDeVeiculos;

    public VeiculosAdapter(List<Veiculos> lista ) {
        this.listaDeVeiculos = lista;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemLista = LayoutInflater.from(parent.getContext())
                                        .inflate(R.layout.lista_veiculos_adapter, parent, false);

        return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {

        Veiculos veiculo = listaDeVeiculos.get(position);
        myViewHolder.veiculos.setText(veiculo.getNomeVeiculo());

    }

    @Override
    public int getItemCount() {

        return this.listaDeVeiculos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView veiculos;

        public MyViewHolder(View itemView) {
            super(itemView);

            veiculos = itemView.findViewById(R.id.idTextVeiculo);
        }
    }
}
