package com.example.ep2transportadora.Activity.model;

import java.io.Serializable;

public class Veiculos implements Serializable {

    private Long id;
    private String nomeVeiculo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeVeiculo() {
        return nomeVeiculo;
    }

    public void setNomeVeiculo(String nomeVeiculo) {
        this.nomeVeiculo = nomeVeiculo;
    }
}

